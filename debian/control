Source: namecoin
Section: utils
Priority: optional
Maintainer: Masayuki Hatta <mhatta@debian.org>
Build-Depends:
 autotools-dev,
 bash-completion,
 debhelper,
 dh-autoreconf,
 dh-exec,
 libboost-chrono-dev,
 libboost-filesystem-dev,
 libboost-system-dev,
 libboost-test-dev,
 libboost-thread-dev,
 libdb++-dev,
 libevent-dev,
 libleveldb-dev,
 libminiupnpc-dev,
 libprotobuf-dev,
 libqrencode-dev,
 libsecp256k1-dev,
 libssl-dev,
 libunivalue-dev (>= 1.0.4),
 libzmq3-dev,
 pkg-config,
 protobuf-compiler,
 python3,
 qt5-qmake,
 qtbase5-dev,
 qttools5-dev-tools
Standards-Version: 4.4.1
Homepage: https://www.namecoin.org/
Vcs-Git: https://salsa.debian.org/debian/namecoin.git
Vcs-Browser: https://salsa.debian.org/debian/namecoin
Rules-Requires-Root: no

Package: namecoind
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: decentralized information registration and transfer system - daemon
 This package provides Namecoin Core daemon namecoind,
 and command-line tool namecoin-cli to interact with the daemon.
 .
 Namecoin is a decentralized open source information registration and
 transfer system based on the Bitcoin cryptocurrency. It was first to
 implement merged mining and a decentralized DNS. Namecoin Core is the
 reference implementation of Namecoin.
 .
 Beware that unless pruning is enabled,
 full global transaction history is stored locally at each client,
 which may require large amounts of disk space
 (hundreds of gigabyte).

Package: namecoin-qt
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 bash-completion,
Description: decentralized information registration and transfer system - GUI
 This package provides Namecoin Core tool Namecoin-Qt,
 a graphical user interface for Namecoin.
 .
 Namecoin is a decentralized open source information registration and
 transfer system based on the Bitcoin cryptocurrency. It was first to
 implement merged mining and a decentralized DNS. Namecoin Core is the
 reference implementation of Namecoin.
 .
 Namecoin Core is the reference implementation of Namecoin.
 .
 Beware that unless pruning is enabled,
 full global transaction history is stored locally at each client,
 which may require large amounts of disk space
 (hundreds of gigabyte).

Package: namecoin-tx
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 bash-completion,
Description: decentralized information registration and transfer system - transaction tool
 This package provides Namecoin Core tool namecoin-tx,
 a command-line tool for creating, parsing, or modifying transactions.
 .
 Namecoin is a decentralized open source information registration and
 transfer system based on the Bitcoin cryptocurrency. It was first to
 implement merged mining and a decentralized DNS. Namecoin Core is the
 reference implementation of Namecoin.
 .
 Beware that unless pruning is enabled,
 full global transaction history is stored locally at each client,
 which may require large amounts of disk space
 (hundreds of gigabyte).
